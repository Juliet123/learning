﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Learning2
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var db = new Context())
            {
                Movie movieobject = new Movie { MovieID = 1, Name = "Blank Panther", Rank = "One",Year = "2018"};
                db.Movies.Add(movieobject);
                db.SaveChanges();
            }
        }
    }
}
