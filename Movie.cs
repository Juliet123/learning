﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Learning2
{
    class Movie
    {
        public int MovieID { get; set; }
        [Key]
        public string Rank { get; set; }
        public string Name { get; set; }
        public string Year { get; set; }
    }
}
